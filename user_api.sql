-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: May 25, 2016 at 01:22 PM
-- Server version: 5.5.49-cll
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hnwtvcco_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_api`
--

CREATE TABLE IF NOT EXISTS `user_api` (
  `ID_UserApi` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `api_key` varchar(255) NOT NULL,
  `total_request` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID_UserApi`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `user_api`
--

INSERT INTO `user_api` (`ID_UserApi`, `username`, `password`, `api_key`, `total_request`) VALUES
(13, 'dsa', 'dsa', 'nKT6PrEcZyXJqCVviQOw13', 1),
(17, 'henwen', 'henwen', 'ExYctdZGA7HQWaXozurM14', 0),
(18, 'yonathankevin', 'p', 'tLRJEVQGkZNpmB7sxq3218', 0),
(19, 'yonathankevin', 'zxcvbnm1', '7cBXDawjrOECIlpMmz8n19', 7),
(20, 'felice', 'felice123', 'Heqi0PObv4Gp65RBz9Uo20', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
