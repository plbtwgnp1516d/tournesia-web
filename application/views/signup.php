<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tournesia - Daftar</title>
	<link rel="icon" href="<?php echo base_url(); ?>/assets/img/logo.png"/>

    <!-- Bootstrap Core CSS -->
   <link href="<?php echo base_url(); ?>/assets/signup/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>/assets/signup/css/agency.css" rel="stylesheet">
	
	<link href="<?php echo base_url(); ?>/assets/signup/img/favicon.jpg">
    <!-- Custom Fonts -->
   <link href="<?php echo base_url(); ?>/assets/signup/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
	
	<link href="<?php echo base_url(); ?>/assets/signup/css/login.css" rel="stylesheet" >

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<div class="container">
<div class="col-lg-12"><br/><br/><br/>
<div class="jumbotron">
		<fieldset>
				
		<div class="row  text-center">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Daftar Baru</h2>
                    <h3 class="section-subheading text-muted"></h3><br/>
                </div>
        </div>
		
		<?php echo form_open('Welcome/daftar'); ?>
		<form method="POST">
		
		<div class="row">
			<div class="form-group">
				 <label for="username">Username</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="username" id="username" placeholder="Masukkan username" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
			</div>
		</div>	
		
		<br/>
		<div class="row">
			<div class="form-group">
				<label for="password">Password</label>
                    <div class="input-group">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Masukkan password" required >
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
			</div>
		</div>
		<br/>

		<br/>
		
		<div class="row">
			<div class="form-group">
				<div class="row">
				<div class="col-lg-12 text-center">
				<input type="submit" value="Daftar" class="btn btn-xl">
			</div>
		</div>
		
		</form>
		 <?php echo form_close(); ?>
		
		</div>
		</div>
		<br/>
		</fieldset>
</div>
</div>
</div>
	
	<br/><br/><br/><br/>
	<div class="row centered-form">

 <footer>
        <div class="container">
            <div class="row">
                
            </div>
        </div>
    </footer>
</body>
</html>