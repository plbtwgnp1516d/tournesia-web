<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Tournesia</title>
  <link rel="icon" href="<?php echo base_url(); ?>/assets/img/logo.png"/>
<!--
Holiday Template
http://www.templatemo.com/tm-475-holiday
-->
	
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700' rel='stylesheet' type='text/css'>
  <link href="<?php echo base_url(); ?>/assets/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/assets/css/flexslider.css" rel="stylesheet" >
  <link href="<?php echo base_url(); ?>/assets/css/templatemo-style.css" rel="stylesheet">
   <link href="<?php echo base_url(); ?>/assets/css/login.css" rel="stylesheet" >

 

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body class="tm-gray-bg">
  	<!-- Header -->
  	<div class="tm-header">
  		<div class="container">
  			<div class="row">
  				<div class="col-lg-6 col-md-4 col-sm-3 tm-site-name-container">
  					<a href="#" class="tm-site-name"><img src="<?php echo base_url(); ?>/assets/img/icon.png" alt="Image" />	TOURNESIA</a>	
  				</div>
	  			<div class="col-lg-6 col-md-8 col-sm-9">
	  				<div class="mobile-menu-icon">
		              <i class="fa fa-bars"></i>
		            </div>
	  				<nav class="tm-nav">
						<ul>
							<li><a href="<?php echo site_url('welcome/index')?>" class="active">Halaman Utama</a></li>
							<li><a href="<?php echo site_url('welcome/signup')?>">Daftar</a></li>
							<li><a href="" data-toggle="modal" data-target="#myModal">Login</a></li>
						</ul>
					</nav>		
	  			</div>				
  			</div>
  		</div>	  	
  	</div>
	
	<!-- Banner -->
	<section class="tm-banner">
		<!-- Flexslider -->
		<div class="flexslider flexslider-banner">
		  <ul class="slides">
		    <li>
			    <div class="tm-banner-inner">
					<h1 class="tm-banner-title">Jelajah <span class="tm-yellow-text">Indonesia</span></h1>
					<p class="tm-banner-subtitle">Berbagi Bersama</p>	
				</div>
				<img src="<?php echo base_url(); ?>/assets/img/rajaampat.jpg" alt="Image" />	
		    </li>
		    <li>
			    <div class="tm-banner-inner">
					<h1 class="tm-banner-title">API Wisata <span class="tm-yellow-text">Indonesia</span> </h1>
					<p class="tm-banner-subtitle">Daftar Sekarang</p>
				</div>
		      <img src="<?php echo base_url(); ?>/assets/img/bromo.jpg" alt="Image" />
		    </li>
		  </ul>
		</div>	
	</section>

	<!-- gray bg -->	
	<section class="container tm-home-section-1" id="more">
		<div class="row">
			<!-- slider -->
			<div class="flexslider flexslider-about effect2">
			  <ul class="slides">
			    <li>
			      <img src="<?php echo base_url(); ?>/assets/img/komodo.png" alt="image" />
			      <div class="flex-caption">
			      	<h2 class="slider-title">Tournesia API</h2>
			      	<h3 class="slider-subtitle">Tournesia menyediakan REST API yang dapat digunakan untuk mengambil data wisata di seluruh Indonesia.</h3>
			      	<p class="slider-description">Data-data wisata dikumpulkan secara kolaboratif bersama-sama dengan menggunakan aplikasi android Tournesia. Validasi data
					dapat dilihat dengan menggunakan votes yang ada dalam fitur aplikasi.
					<br><br>
					Data tersebut akan diberikan kepada anda secara terbuka dalam JSON Format.</p>
			      </div>			      
			    </li>
			    <li>
			      <img src="<?php echo base_url(); ?>/assets/img/toba.png" alt="image" />
			      <div class="flex-caption">
			      	<h2 class="slider-title">Spesifikasi</h2>
			      	<h3 class="slider-subtitle">Ketentuan akses dan autentikasi API</h3>
			      	<p class="slider-description">Setelah mendaftar, setiap pengguna akan diberikan API Key unik sebagai password yang digunakan untuk memanggil API Tournesia.<br><br>
					Dalam pemanggilan API letakan api key pada URL seperti berikut : <br><br>
					<a href="hnwtvc.com/tournesia-rest/index.php/public/wisata/konten?api_key=YOUR_API_KEY">hnwtvc.com/tournesia-rest/index.php/public/wisata/konten?api_key=YOUR_API_KEY</a>
					</p>
			      </div>			      
			    </li>
			    <li>
			      <img src="<?php echo base_url(); ?>/assets/img/borobudur.png" alt="image" />
			      <div class="flex-caption">
			      	<h2 class="slider-title">Buat Aplikasi</h2>
			      	<h3 class="slider-subtitle">Bersama-sama memperluas wawasan wisata Indonesia.</h3>
			      	<p class="slider-description">Dengan mengembangkan aplikasi baru menggunakan API Tournesia. Anda telah membantu kami untuk meningkatkan nilai pariwisata Indonesia. <br><br>
					<a href="<?php echo site_url('welcome/signup')?>">Daftar Sekarang</a></p>
			      </div>			      
			    </li>
			  </ul>
			</div>
		</div>
	</section>
	
	<!-- gray bg -->	
	<section class="container tm-home-section-1" id="more">
		

			<!--<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="tm-home-box-1 tm-home-box-1-2 tm-home-box-1-center">
					<img src="<?php echo base_url(); ?>/assets/img/index-01.jpg" alt="image" class="img-responsive">
					<a href="#">
						<div class="tm-green-gradient-bg tm-city-price-container">
							<span>New York</span>
							
						</div>	
					</a>			
				</div>				
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="tm-home-box-1 tm-home-box-1-2 tm-home-box-1-right">
					<img src="<?php echo base_url(); ?>/assets/img/index-02.jpg" alt="image" class="img-responsive">
					<a href="#">
						<div class="tm-red-gradient-bg tm-city-price-container">
							<span>Paris</span>
							
						</div>	
					</a>					
				</div>				
			</div>-->
		</div>
	
		<div class="section-margin-top">
			<div class="row">				
				<div class="tm-section-header">
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>
					<div class="col-lg-6 col-md-6 col-sm-6"><h2 class="tm-section-title">Aplikasi dengan Tournesia</h2></div>
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>	
				</div>
			</div>
			<div class="row">

				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12">
					<div class="tm-home-box-2">						
					    <img src="<?php echo base_url(); ?>/assets/img/logo.png" alt="image" class="img-responsive">
						<h3>Tournesia [Android]</h3>
						<p class="tm-date">Sejak : 22 Mei 2016</p>

					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12">
					<div class="tm-home-box-2 tm-home-box-2-right">						
					    <img src="<?php echo base_url(); ?>/assets/img/logo2.png" alt="image" class="img-responsive">
						<h3>Lacak Indonesia [Android]</h3>
						<p class="tm-date">Sejak : 5 Juni 2016</p>

					</div>
				</div>
			</div>
			
		</div>
	</section>		
	

	<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
   
     
	  
		<div class="login">
		
		  <h2 class="login-header">Login</h2>
		
		  <?php $attributes = array('class' => 'login-container'); ?>
		   <?php echo form_open('Welcome/verifylogin',$attributes); ?>
		   <form method="POST">
			<p><input type="text" name="username" placeholder="Username"></p>
			<p><input type="password" name="password" placeholder="Password"></p>
			<p><input type="submit" value="MASUK"></p>
			</form>
		  <?php echo form_close(); ?>
		</div>
		
		
		
      
     
    </div>
  </div>
	<footer class="tm-black-bg">
		<div class="container">
			<div class="row">
				<p class="tm-copyright-text">Copyright &copy; 2016 Tournesia
                
                | Designed by PLBTW 2016</a></p>
			</div>
		</div>		
	</footer>
	<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery-1.11.2.min.js"></script>      		<!-- jQuery -->
  	<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/moment.js"></script>							<!-- moment.js -->
	<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>					<!-- bootstrap js -->
	<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap-datetimepicker.min.js"></script>	<!-- bootstrap date time picker js, http://eonasdan.github.io/bootstrap-datetimepicker/ -->
	<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.flexslider-min.js"></script>
<!--
	<script src="js/froogaloop.js"></script>
	<script src="js/jquery.fitvid.js"></script>
-->
   	<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/templatemo-script.js"></script>      		<!-- Templatemo Script -->
	<script>
		// HTML document is loaded. DOM is ready.
		$(function() {

			$('#hotelCarTabs a').click(function (e) {
			  e.preventDefault()
			  $(this).tab('show')
			})

        	$('.date').datetimepicker({
            	format: 'MM/DD/YYYY'
            });
            $('.date-time').datetimepicker();

			// https://css-tricks.com/snippets/jquery/smooth-scrolling/
		  	$('a[href*=#]:not([href=#])').click(function() {
			    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			      var target = $(this.hash);
			      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			      if (target.length) {
			        $('html,body').animate({
			          scrollTop: target.offset().top
			        }, 1000);
			        return false;
			      }
			    }
		  	});
		});
		
		// Load Flexslider when everything is loaded.
		$(window).load(function() {	  		
			// Vimeo API nonsense

/*
			  var player = document.getElementById('player_1');
			  $f(player).addEvent('ready', ready);
			 
			  function addEvent(element, eventName, callback) {
			    if (element.addEventListener) {
			      element.addEventListener(eventName, callback, false)
			    } else {
			      element.attachEvent(eventName, callback, false);
			    }
			  }
			 
			  function ready(player_id) {
			    var froogaloop = $f(player_id);
			    froogaloop.addEvent('play', function(data) {
			      $('.flexslider').flexslider("pause");
			    });
			    froogaloop.addEvent('pause', function(data) {
			      $('.flexslider').flexslider("play");
			    });
			  }
*/

			 
			 
			  // Call fitVid before FlexSlider initializes, so the proper initial height can be retrieved.
/*

			  $(".flexslider")
			    .fitVids()
			    .flexslider({
			      animation: "slide",
			      useCSS: false,
			      animationLoop: false,
			      smoothHeight: true,
			      controlNav: false,
			      before: function(slider){
			        $f(player).api('pause');
			      }
			  });
*/


			  

//	For images only
		    $('.flexslider').flexslider({
			    controlNav: false
		    });


	  	});
	</script>
 </body>
 </html>